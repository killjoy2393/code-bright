import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    comments: [],
    comment: {}
  },
  mutations: {
    SET_COMMENTS: (state, comments) => {
      state.comments = comments
    },
    SET_COMMENT: (state, comment) => {
      state.comment = comment
    }
  },
  getters: {
    COMMENTS: state => {
      return state.comments;
    },
    COMMENT: state => {
      return state.comment;
    }
  },

  actions: {
    GET_COMMENTS: ctx => {
      axios
      .get('https://5cbef81d06a6810014c66193.mockapi.io/api/comments')
      .then(response => (ctx.commit('SET_COMMENTS', response.data)))
    },

    GET_COMMENT: (ctx, id) => {
      axios
      .get(`https://5cbef81d06a6810014c66193.mockapi.io/api/comments/${id}`)
      .then(response => (ctx.commit('SET_COMMENT', response.data)))
      .catch(e => {console.log(e)})
    },

    ADD_COMMENT: (ctx, body) => {
      axios
      .post('https://5cbef81d06a6810014c66193.mockapi.io/api/comments', body)
      .catch(e => {console.log(e)})
    },

    EDIT_COMMENT: (ctx, payload) => {
      axios
      .put(`https://5cbef81d06a6810014c66193.mockapi.io/api/comments/${payload.id}`, payload.body)
      .catch(e => {console.log(e)})
    },

    DELETE_COMMENT: (ctx, id) => {
      axios
      .delete(`https://5cbef81d06a6810014c66193.mockapi.io/api/comments/${id}`)
      .catch(e => {console.log(e)})
    }
  }
})
